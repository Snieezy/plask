go
CREATE VIEW WystawyAll AS
SELECT Wystawy.Tytul, Wystawy.Data_utworzenia, Wystawy.Ilosc_miejsc, Wystawy.Powierzchnia_scian, Wystawy.Powierzchnia_podlogi, Wystawy.Czas_zwiedzania, Wystawy.PESEL, Wystawy_okresowe.Data_otwarcia, Wystawy_okresowe.Data_zakonczenia 
FROM Wystawy
LEFT JOIN Wystawy_okresowe 
ON (Wystawy.Tytul = Wystawy_okresowe.Tytul AND Wystawy.Data_utworzenia = Wystawy_okresowe.Data_utworzenia)
go

/*
^ Wybierz wszystkie parametry(wystaw okresowych) z wystaw po��czonych z wystawami okresowymi na podstawie klucza naturalnego, tam gdzie parametr nie wyst�puje, wstaw NULL
(normalne wystawy b�d� mia�y nulle w dacie otwarcia i zako�czenia)
*/
 
SELECT Pracownicy.Imie, Pracownicy.Nazwisko, Data_utworzenia, Czas_zwiedzania, Data_otwarcia, Data_zakonczenia 
FROM (WystawyAll
INNER JOIN Pracownicy
ON (WystawyAll.PESEL=Pracownicy.PESEL)
)
/*
wybierz dane tw�rc�w wystaw, i parametry wystawy z po��czenia widoku i tabeli Pracownicy, po��czenie na podstawie klucza -> numeru PESEL tw�rcy
*/
-----------------------------------------------

SELECT Artysci.Imie, Artysci.Nazwisko, COUNT(Artysci.ID) AS NumberOfWorks --wybierz imi� i nazwisko artysty, policz wyst�pienie jego id
FROM (Dziela --z dzie� 
INNER JOIN Artysci -- po��czonych z artystami
ON Dziela.IDArtysty = Artysci.ID) --na podstawie przyr�wnania ID zawartych w Dzie�ach, do id artyst�w
GROUP BY Imie, Nazwisko --po��cz te rekordy, kt�re maj� to samo imi� i nazwisko w jeden
ORDER BY NumberOfWorks DESC; --wynik posegreguj malej�co

-----------------------------------------------

SELECT Bilety.Data_i_godzina, SUM(Ilosc) AS IloscBiletow, Wystawy.Tytul, Wystawy.Data_utworzenia, Wystawy.PESEL --wybierz dat� i godzin� kupienia biletu, zsumuj ilo��, podaj tytu�, dat�, i pesel tw�rcy wystawy
INTO #tmp --zapisz w tymczasowej tabeli
FROM Bilety, Wystawy, Bilety_Wystawy --z podanych tabel
WHERE Bilety_Wystawy.Data_i_godzina = Bilety.Data_i_godzina AND Bilety_Wystawy.Data_utworzenia=Wystawy.Data_utworzenia AND Bilety_Wystawy.Tytul=Wystawy.Tytul --gdzie pasuje ich klucz naturalny
GROUP BY Bilety.Data_i_godzina, Wystawy.Tytul, Wystawy.Data_utworzenia, Wystawy.PESEL --pogrupuj wg podanych atrybut�w

SELECT Tytul, SUM(IloscBiletow) AS Total, Imie, Nazwisko --wybierz tytu� wystawy, sum� sprzedanych na ni� bilet�w i dane pracownika, kt�ry j� stworzy�
FROM #tmp, Pracownicy --z tabeli tymczasowej i tabeli Pracownicy
WHERE #tmp.PESEL=Pracownicy.PESEL --gdzie pasuje ich klucz
GROUP BY Tytul, Imie, Nazwisko --pogrupuj wg Tytu�u wystawy i danych tw�rcy
ORDER BY Total DESC; --posegreguj malej�co

--jako pierwszy wynik b�dzie podany pracownik, k�ry stworzy� najbardziej popularn� wystaw� (dalej malej�co) (scenariusz)

------------------------------------------------

SELECT Dziela.IDArtysty, Artysci.Imie, Artysci.Nazwisko,Dziela.Tytul,  Dziela.Data_utworzenia,  Miejsca_pobytu.Od_kiedy, Miejsca_pobytu.Do_kiedy, Miejsca_pobytu.Nazwa --wybierz nast�puj�ce atrybuty IdArtysty, jego imi�, etc.
FROM (Miejsca_pobytu  --z miejsc pobytu
INNER JOIN (Dziela  --po��czonych z dzie�ami spe�niaj�cymi warunki:
	INNER JOIN Artysci  --i jeszcze po��czonych z Artystami na warunkach:
		ON (Dziela.IDArtysty=Artysci.ID --dzielo na pewno nalezy do danego artysty
			AND Artysci.Kraj = 'Polska' --artysta pochodzi z Polski
			AND YEAR(Dziela.Data_utworzenia)>1800 --stworzy� dzie�o w XIXw.
			AND YEAR(Dziela.Data_utworzenia)<1901
			AND (Dziela.Typ='Malarstwo' --ma okre�lony typ
				OR Dziela.Typ='Rysunek' 
				OR Dziela.Typ='Grafika' 
				OR Dziela.Typ='Rze�ba')
			))
	ON (Miejsca_pobytu.IDArtysty = Dziela.IDArtysty --dzielo na pewno jest w danym miejscu (por�wnanie klucza naturalnego)
		AND Miejsca_pobytu.Data_utworzenia = Dziela.Data_utworzenia 
		AND Miejsca_pobytu.Tytul = Dziela.Tytul
		--najpierw data od-kiedy potem pocz�tek wystawy
		AND DATEDIFF(day, Miejsca_pobytu.Od_kiedy, '2014-06-26') >= 0 --znajduje si� na magazynie w okre�lonym przedziale czasu
		AND DATEDIFF(day, '2015-08-28', Miejsca_pobytu.Do_kiedy)>= 0
		AND Miejsca_pobytu.Nazwa='Magazyn'

	)
);--sztuka polska XIXw.(scenariusz)
-------------------------------------------------
-- �rednia cena ka�dego typu bilet�w, kt�ra ma by� wpisana do cennika na noc muze�w(scenariusz)
SELECT Nazwa, FLOOR(AVG(Cena)*0.2) AS ostateczna_cena  FROM Typy_biletow  --wybierz nazw� typu i podaj jego cen� po 80% obni�ce, kwoty zaokr�glone w d�, do pe�nych liczb
GROUP BY Nazwa
ORDER BY ostateczna_cena --posegreguj wg wynikowej ceny biletu

-------------------------------------------------
SELECT Dziela.Tytul  --wybierz tytul dzie�a
FROM Dziela, Miejsca_pobytu --z tabel dzie�a i miejsca pobytu
WHERE Dziela.Tytul =Miejsca_pobytu.Tytul AND Dziela.Data_utworzenia=Miejsca_pobytu.Data_utworzenia AND Dziela.IDArtysty=Miejsca_pobytu.IDArtysty --gdzie pasuje klucz naturalny
AND EXISTS (
	SELECT Tytul,Data_utworzenia  --wybierz tytu� i dat� utworzenia
	FROM #tmp --z tymczasowej tablicy
	WHERE IloscBiletow in ( --gdzie ilo�� sprzedanych bilet�w jest najwi�ksza
		SELECT max(IloscBiletow) 
		FROM #tmp) 
		AND Miejsca_pobytu.Data_utworzenia_wystawy=#tmp.Data_utworzenia AND Miejsca_pobytu.Tytul_wystawy=#tmp.Tytul
		)
/*
^ zapytanie wy�wietli dzie�a, kt�re znajdowa�y si� na najbardziej popularnej wystawie
*/
--------------------------------------------------

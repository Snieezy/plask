

INSERT INTO Artysci (Imie, Nazwisko, Adres, Kraj) VALUES
('Salvador','Dali',	'Figueres',	'Hiszpania'),
('Pablo','Picasso','Madryt','Hiszpania'),
('Donato','Bardi','Florencja','Włochy'),
('Leonardo','Da Vinci','Anchiano','Włochy'),
('William','Turner','Londyn','Anglia'),
('Jacek','Malczewski','Radom','Polska'),
('Michelangelo','Buonarroti','Caprese','Włochy'),
('Giovanni Lorenzo','Bernini','Neapol','Włochy'),
('Marcel','Duchamp','Blainville-Crevon','Francja');

--Dzieło: ID Artysty, Tytuł Dzieła, Data utw., czy sprz(bool), wartość, {rys, mal, rz, graf}, technika(inst=0, rz=0), dł i sz, wys(inst, rz not null), materiał, styl(epoka)
	--Dali
	INSERT INTO Dziela (IDArtysty, Tytul, Data_utworzenia, Czy_na_sprzedaz, Wartosc, Typ, Technika, Wymiary, Wysokosc, Material, Rodzaj, Sposob_wystawienia) VALUES
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Salvador' AND Artysci.Nazwisko = 'Dali' AND Artysci.Adres = 'Figueres' )),'Miękka konstrukcja z gotowaną fasolką - przeczucie wojny domowej', '1936-01-01',FALSE,2500000,'Malarstwo','Olej','99,9x100',NULL,'Plotno','Surrealizm', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Salvador' AND Artysci.Nazwisko = 'Dali' AND Artysci.Adres = 'Figueres' )),'Trwalosc pamieci','1931-01-01',FALSE,3000000,'Malarstwo','Olej','24x33',NULL,'Plotno','Surrealizm', NULL),	
	--Picasso
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Pablo' AND Artysci.Nazwisko = 'Picasso' AND Artysci.Adres = 'Madryt' )), 'Guernica', '1937-01-01', TRUE,35000000, 'Malarstwo', 'Olej', '349x776', NULL, 'Płótno','Kubizm', NULL),
	--Donatello
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Donato' AND Artysci.Nazwisko = 'Bardi' AND Artysci.Adres = 'Florencja' )), 'Dawid', '1440-01-01', FALSE,2340000, 'Rzeźba', NULL, '15x20', 50, 'Brąz','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Donato' AND Artysci.Nazwisko = 'Bardi' AND Artysci.Adres = 'Florencja' )), 'Maria Magdalena', '1457-01-01', FALSE,5500000, 'Rzeźba', NULL, '35x50', 160, 'Drewno','Renesans', NULL),
	--Leonardo
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano' )), 'portret Ginewry Benci', '1478-01-01', TRUE,5600000, 'Malarstwo', 'Olej', '38x37', NULL, 'Drewno','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano' )), 'Jan Chrzciciel', '1516-01-01', FALSE,6600000, 'Malarstwo', 'Olej', '69x57', NULL, 'Drewno','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano' )), 'Madonna Litta', '1491-01-01', TRUE,5600000, 'Malarstwo', 'Olej', '42x33', NULL, 'Drewno','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano' )), 'św. Anna Samotrzecia', '1508-01-01', FALSE,7600000, 'Malarstwo', 'Olej', '168x112', NULL, 'Drewno','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano' )), 'Autoportret', '1513-01-01',TRUE,9999000, 'Rysunek', 'Ołówek', '33,2x21,2', NULL, 'Papier','Renesans', NULL),
	--Turner
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'William' AND Artysci.Nazwisko = 'Turner' AND Artysci.Adres = 'Londyn' )), 'Ostatni rejs Temerairea', '1839-01-01', FALSE,10000000,'Malarstwo','Olej','91x122',NULL,'Płótno','Romantyzm', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'William' AND Artysci.Nazwisko = 'Turner' AND Artysci.Adres = 'Londyn' )), 'Szybkość, para, deszcz', '1844-01-01', FALSE,9000000,'Malarstwo','Olej','91x122',NULL,'Płótno','Romantyzm', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'William' AND Artysci.Nazwisko = 'Turner' AND Artysci.Adres = 'Londyn' )), 'The Burning of the Houses of Parliament', '1835-01-01', FALSE,7500000,'Malarstwo','Olej','93x123',NULL,'Płótno','Romantyzm', NULL),
	--Malczewski
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Jacek' AND Artysci.Nazwisko = 'Malczewski' AND Artysci.Adres = 'Radom' )), 'Błędne koło', '1897-01-01', TRUE,7400000,'Malarstwo','Olej','174x240',NULL,'Płótno','Symbolizm', NULL),
	--Anioł
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Michelangelo' AND Artysci.Nazwisko = 'Buonarroti' AND Artysci.Adres = 'Caprese' )), 'Dawid', '1504-01-01', FALSE,7340000, 'Rzeźba', NULL, '100x110', 434, 'Marmur Karraryjski','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Michelangelo' AND Artysci.Nazwisko = 'Buonarroti' AND Artysci.Adres = 'Caprese' )), 'Mojżesz', '1515-01-01', FALSE,7840000, 'Rzeźba', NULL, '100x110', 300, 'Marmur','Renesans', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Michelangelo' AND Artysci.Nazwisko = 'Buonarroti' AND Artysci.Adres = 'Caprese' )), 'Jeniec', '1513-01-01', TRUE,8740000, 'Rzeźba', NULL, '50x70', 228, 'Marmur','Renesans', NULL),
	--Bernini
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Giovanni Lorenzo' AND Artysci.Nazwisko = 'Bernini' AND Artysci.Adres = 'Neapol' )), 'Apollo i Dafne', '1625-01-01', FALSE,8540000, 'Rzeźba', NULL, '100x120', 243, 'Marmur','Barok', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Giovanni Lorenzo' AND Artysci.Nazwisko = 'Bernini' AND Artysci.Adres = 'Neapol' )), 'Dawid', '1624-01-01', TRUE,9040000, 'Rzeźba', NULL, '80x90', 170, 'Marmur','Barok', NULL),
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Giovanni Lorenzo' AND Artysci.Nazwisko = 'Bernini' AND Artysci.Adres = 'Neapol' )), 'Popiersie kardynała Scipione Borgese', '1632-01-01', FALSE,9460000, 'Rzeźba', NULL, '50x45', 78, 'Marmur','Barok', NULL),
	--Duchamp
	((SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Marcel' AND Artysci.Nazwisko = 'Duchamp' AND Artysci.Adres = 'Blainville-Crevon' )), ' Fontanna', '1917-01-01', FALSE,960000, 'Instalacja', NULL, '45x30', 50, 'Metal','Dadaizm','Brak');

--sposób wystawienia: 
	



--Cenniki: Od_kiedy, Do_kiedy
	INSERT INTO Cenniki VALUES
	(
		'2014-06-13', '2020-10-05' --malarstwo xix w.
	)--1
	INSERT INTO Cenniki VALUES
	(
		'2014-06-13', '2015-01-18' --rzeźba renesansu
	)--2
	INSERT INTO Cenniki VALUES
	(
		'2014-08-17', '2015-03-03' --tw. leonarda
	)--3
	INSERT INTO Cenniki VALUES
	(
		'2014-06-13', '2015-01-18' --dla mal xix wieku i rz. r.
	)--4
	INSERT INTO Cenniki VALUES
	(
		'2014-08-17', '2015-01-18' --wszystkie na jednym
	)--5
	INSERT INTO Cenniki VALUES
	(
		'2014-08-17', '2015-03-03' --mal i tw. leo.
	)--6
--Typy_biletow: nazwa, IDCennika, Numer w cenniku, Cena
	--1
	INSERT INTO Typy_biletow VALUES
	(
		'Normalny', 1, 1, 40.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Ulgowy', 1, 2, 20.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Specjalny', 1, 3, 2.00
	)
	--2
	INSERT INTO Typy_biletow VALUES
	(
		'Normalny', 2, 1, 50.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Ulgowy', 2, 2, 25.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Specjalny', 2, 3, 2.00
	)
	--3
	INSERT INTO Typy_biletow VALUES
	(
		'Normalny', 3, 1, 60.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Ulgowy', 3, 2, 30.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Specjalny', 3, 3, 2.00
	)
	--4
	INSERT INTO Typy_biletow VALUES
	(
		'Normalny', 4, 1, 80.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Ulgowy', 4, 2, 40.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Specjalny', 4, 3, 2.00
	)
	--5
	INSERT INTO Typy_biletow VALUES
	(
		'Normalny', 5, 1, 100.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Ulgowy', 5, 2, 50.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Specjalny', 5, 3, 4.00
	)
	INSERT INTO Typy_biletow VALUES
	(
		'Wycieczkowy',5, 4, 40.00
	)
--Pracownicy: PESEL, Imię, Nazwisko


--Bilety: Data_i_godzina SMALLDATETIME PRIMARY KEY,Ilosc INT NOT NULL,Rezerwacja BIT NOT NULL,Nazwa VARCHAR(100) NOT NULL,IDCennika INT,
	INSERT INTO Bilety VALUES
	('2014-08-12 12:35:29', 14, 0, 'Wycieczkowy', 5),
	('2014-08-13 19:45:22', 20, 0, 'Wycieczkowy', 5),
	('2014-09-10 13:35:21', 1, 0, 'Normalny', 1),
	('2014-10-12 12:38:26', 25, 0, 'Specjalny', 4);

INSERT INTO Pracownicy VALUES
(80071072858,'Miłosz','Kaczmarek'),
(79011128681,'Lidia','Król'),
(70052379212,'Grzegorz','Grabowski');


--Miejsca pobytu: IDArtysty, Tytuł dzieła, Data utw., Od_kiedy, Do_kiedy, nazwa miejsca

--Wystawy: Tytuł wystawy, Data utw., Ilość miejsc, pow ścian, pow podłogi, czas zwiedzania

	INSERT INTO Wystawy VALUES
	('Malarstwo XIX w.','2014-06-13', 174, 300, 180, '00:40:00',79011128681);
	
--Wystawy_okr(IS_A Wystawy): Tytuł wystawy, Data utw., Ilość miejsc, pow ścian, pow podłogi, czas zwiedzania, Data_otwarcia, Data zakończenia
	EXEC dbo.dodaj_wystawy_okresowe 'Rzeźba renesansu','2014-06-13', 174, 300, 180, '00:40:00',80071072858,'2014-06-13','2015-01-18';
	EXEC dbo.dodaj_wystawy_okresowe 'Twórczość Leonarda','2014-08-17', 60, 260, 173, '01:45:00',70052379212,'2014-08-17', '2015-03-03';


--insert into Miejsca_pobytu VALUES (@IDArtysty, @Tytul, @Data_utworzenia, @Data_pocz, @Data_kon, @Nazwa_wys, @Tytul_wystawy, @Data_utworzenia_wystawy)
	EXEC dbo.wstaw_dziela_typ_rodzaj 'Rzeźba','Renesans', '2014-06-13', '2015-01-18', 'Wystawa', 'Rzeźba renesansu', '2014-06-13';

	DECLARE @ID_Artysty INT
	SET @ID_Artysty = (SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Leonardo' AND Artysci.Nazwisko = 'Da Vinci' AND Artysci.Adres = 'Anchiano'))
	EXEC dbo.wstaw_dziela_artysta @ID_Artysty, '2014-08-17', '2015-03-03', 'Wystawa', 'Twórczość Leonarda', '2014-08-17';


	SET @ID_Artysty = (SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Giovanni Lorenzo' AND Artysci.Nazwisko = 'Bernini' AND Artysci.Adres = 'Neapol'))
	EXEC dbo.wstaw_dziela_artysta @ID_Artysty, '2012-04-09','2020-12-05', 'Muzeum: Luwr', NULL, NULL;

	SET @ID_Artysty = (SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Marcel' AND Artysci.Nazwisko = 'Duchamp' AND Artysci.Adres = 'Blainville-Crevon'))
	EXEC dbo.wstaw_dziela_artysta @ID_Artysty, '2013-06-17','2015-04-12', 'Muzeum: Ermitaż', NULL, NULL;

	SET @ID_Artysty = (SELECT ID FROM Artysci WHERE (Artysci.Imie = 'Pablo' AND Artysci.Nazwisko = 'Picasso' AND Artysci.Adres = 'Madryt'))
	EXEC dbo.wstaw_dziela_artysta @ID_Artysty, '2013-06-13','2015-09-15', 'Muzeum: Reina Sofia', NULL, NULL;



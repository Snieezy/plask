CREATE TABLE Artysci
(
	ID SERIAL PRIMARY KEY,
	Imie VARCHAR (100) NOT NULL,
	Nazwisko VARCHAR (100) NOT NULL,
	Adres VARCHAR (100) NOT NULL,
	Kraj VARCHAR (50) NOT NULL

);

CREATE TABLE Dziela
(
	IDArtysty integer NOT NULL,
	Tytul VARCHAR (300) NOT NULL,
	Data_utworzenia DATE NOT NULL,
	Czy_na_sprzedaz BOOLEAN NOT NULL,
	Wartosc MONEY NOT NULL,
	Typ VARCHAR (100) NOT NULL,
	Technika VARCHAR (100),
	Wymiary VARCHAR(100) NOT NULL,
	Wysokosc INT,
	Material VARCHAR (100) NOT NULL,
	Rodzaj VARCHAR (100) NOT NULL,
	Sposob_wystawienia VARCHAR(8000),
	
	CONSTRAINT fk_dziela FOREIGN KEY (IDArtysty) REFERENCES Artysci (ID) ON DELETE CASCADE,

	PRIMARY KEY (IDArtysty, Tytul, Data_utworzenia)
	
);


CREATE TABLE Pracownicy
(
	PESEL NUMERIC(11,0) PRIMARY KEY, --11 cyfr i 0 po przecinku
	Imie VARCHAR (100) NOT NULL,
	Nazwisko VARCHAR (100) NOT NULL
);


CREATE TABLE Wystawy
(
	Tytul VARCHAR(300) NOT NULL,
	Data_utworzenia DATE NOT NULL,
	Ilosc_miejsc INT NOT NULL,
	Powierzchnia_scian FLOAT NOT NULL,
	Powierzchnia_podlogi FLOAT NOT NULL,
	Czas_zwiedzania TIME NOT NULL,
	PESEL NUMERIC(11,0) REFERENCES Pracownicy ON DELETE CASCADE NOT NULL,
	
	PRIMARY KEY (Tytul, Data_utworzenia)
	
);

CREATE TABLE Wystawy_okresowe
(
	Tytul VARCHAR(300),
	Data_utworzenia DATE NOT NULL,
	
	Data_otwarcia DATE NOT NULL,
	Data_zakonczenia DATE NOT NULL,

	FOREIGN KEY (Tytul, Data_utworzenia) REFERENCES Wystawy ON DELETE CASCADE
	
);


CREATE TABLE Cenniki
(
	ID SERIAL PRIMARY KEY,
	Do_kiedy DATE NOT NULL,
	Od_kiedy DATE NOT NULL
);

CREATE TABLE Typy_biletow
(
	Nazwa VARCHAR(100) NOT NULL,
	IDCennika INT NOT NULL,
	Numer_w_cenniku INT NOT NULL,
	Cena MONEY NOT NULL,

	FOREIGN KEY (IDCennika) REFERENCES Cenniki ON DELETE CASCADE ,
	PRIMARY KEY (Nazwa, IDCennika)
);

CREATE TABLE Bilety
(
	Data_i_godzina TIMESTAMP,
	Ilosc INT NOT NULL,
	Rezerwacja BIT NOT NULL,
	Nazwa VARCHAR(100) NOT NULL,
	IDCennika INT NOT NULL,

	FOREIGN KEY (Nazwa, IDCennika) REFERENCES Typy_biletow ON DELETE CASCADE,
	PRIMARY KEY (Data_i_godzina)
);

CREATE TABLE Bilety_Wystawy
(
	Data_i_godzina TIMESTAMP NOT NULL,
	Tytul VARCHAR(300) NOT NULL,
	Data_utworzenia DATE NOT NULL,

	FOREIGN KEY (Data_i_godzina) REFERENCES Bilety,
	FOREIGN KEY (Tytul, Data_utworzenia) REFERENCES Wystawy,
	
	PRIMARY KEY (Data_i_godzina, Tytul, Data_utworzenia)
); --związek n:n SprzedawaneNa

CREATE TABLE Wystawy_Cenniki
(
	Tytul VARCHAR(300) NOT NULL,
	Data_utworzenia DATE NOT NULL,
	IDCennika INT NOT NULL,

	FOREIGN KEY (IDCennika) REFERENCES Cenniki,
	FOREIGN KEY (Tytul, Data_utworzenia) REFERENCES Wystawy,
	
	PRIMARY KEY (IDCennika, Tytul, Data_utworzenia)
);--związek n:n Na

CREATE TABLE Miejsca_pobytu
(
	IDArtysty INT NOT NULL,
	Tytul VARCHAR(300) NOT NULL,
	Data_utworzenia DATE NOT NULL, 
	Od_kiedy DATE NOT NULL,
	Do_kiedy DATE NOT NULL,
	Nazwa VARCHAR(100) NOT NULL,
	--miejscem pobytu może być wystawa, może nie musi zatem może być tu null
	Tytul_wystawy VARCHAR(300), 
	Data_utworzenia_wystawy DATE,

	FOREIGN KEY (IDArtysty,  Tytul, Data_utworzenia) REFERENCES Dziela ON DELETE CASCADE,
	FOREIGN KEY (Tytul_wystawy, Data_utworzenia_wystawy) REFERENCES Wystawy ON DELETE CASCADE,
	PRIMARY KEY (IDArtysty, Od_kiedy, Tytul, Data_utworzenia)
);
